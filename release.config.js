const { semanticReleaseConfigDefault } = require('@beepbeepgo/semantic-release');
module.exports = semanticReleaseConfigDefault({
  plugins: {
    docker: {
      dockerTags: [
        '{{#if prerelease.[0]}}{{prerelease.[0]}}-dind{{else}}latest-dind{{/if}}',
        '{{version}}-dind'
      ]
    }
  }
});
