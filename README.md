# Docker DIND Image to use in gitlab-ci services

This image is primarily used as the base image for the gitlab runners.

## Getting started

To use, the image path to leverage is:

```docker
FROM registry.gitlab.com/beepbeepgo/public/docker/docker:latest-dind
```
